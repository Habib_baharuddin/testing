import { Component, OnInit } from '@angular/core';

export interface dataSource {
  order: string;
  created: string;
  name: string;
  description: string;
}

const ELEMENT_DATA: dataSource[] = [
  { created: '06-06-2021', name: 'Hydrogen', description: 'description', order: 'A001' },
  { created: '06-07-2021', name: 'Helium', description: 'description', order: 'A002' },
  { created: '16-07-2021', name: 'Lithium', description: 'description', order: 'A003' },
  { created: '18-06-2021', name: 'Beryllium', description: 'description', order: 'A004' },
  { created: '06-06-2021', name: 'Boron', description: 'description', order: 'A005' },
  { created: '06-06-2021', name: 'Carbon', description: 'description', order: 'A006' },
  { created: '06-06-2021', name: 'Nitrogen', description: 'description', order: 'A007' },
  { created: '08-06-2021', name: 'Oxygen', description: 'description', order: 'A008' },
  { created: '06-06-2021', name: 'Fluorine', description: 'description', order: 'A009' },
  { created: '06-06-2021', name: 'Neon', description: 'description', order: 'B001' },
];
@Component({
  selector: 'app-visitor-list',
  templateUrl: './visitor-list.component.html',
  styleUrls: ['./visitor-list.component.css']
})
export class VisitorListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  displayedColumns: string[] = ['created', 'order', 'name', 'description'];
  dataSource = ELEMENT_DATA;
}
