import { Component, OnInit } from '@angular/core';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import JsBarcode from 'jsbarcode/bin/JsBarcode'
pdfMake.vfs = pdfFonts.pdfMake.vfs;
export interface dataSource {
  order: string;
  created: string;
  name: string;
  description: string;
}

const ELEMENT_DATA: dataSource[] = [
  { created: '06-06-2021', name: 'Hydrogen', description: 'description', order: 'A001' },
  { created: '06-07-2021', name: 'Helium', description: 'description', order: 'A002' },
  { created: '16-07-2021', name: 'Lithium', description: 'description', order: 'A003' },
  { created: '18-06-2021', name: 'Beryllium', description: 'description', order: 'A004' },
  { created: '06-06-2021', name: 'Boron', description: 'description', order: 'A005' },
  { created: '06-06-2021', name: 'Carbon', description: 'description', order: 'A006' },
  { created: '06-06-2021', name: 'Nitrogen', description: 'description', order: 'A007' },
  { created: '08-06-2021', name: 'Oxygen', description: 'description', order: 'A008' },
  { created: '06-06-2021', name: 'Fluorine', description: 'description', order: 'A009' },
  { created: '06-06-2021', name: 'Neon', description: 'description', order: 'B001' },
];
@Component({
  selector: 'app-request-queue-number',
  templateUrl: './request-queue-number.component.html',
  styleUrls: ['./request-queue-number.component.css']
})
export class RequestQueueNumberComponent implements OnInit {
  textToBase64Barcode(text) {
    var canvas = document.createElement("canvas");
    JsBarcode(canvas, text, { format: "CODE39" });
    return canvas.toDataURL("image/png");
  }
  generatePdf() {
    const documentDefinition = {
      content: [
        {
          text: 'ALMEGATEX',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          text: 'Nomor Antrian Anda',
          fontSize: 12,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          text: 'A001',
          bold: true,
          fontSize: 60,
          alignment: 'center',
          margin: [0, 10, 0, 20]
        },
        {
          text: 'Mohon menunggu',
          fontSize: 12,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          image: this.textToBase64Barcode("123456789"),
          alignment: 'center'


        },
        {
          text: 'Created : 06-06-2021',
          fontSize: 12,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          text: 'Budayakan antri untuk kenyamanan bersama',
          fontSize: 12,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },

      ]
    };
    pdfMake.createPdf(documentDefinition).open({}, window);
  }
  constructor() { }

  ngOnInit(): void {
  }
  displayedColumns: string[] = ['created', 'order', 'name', 'description'];
  dataSource = ELEMENT_DATA;
}
